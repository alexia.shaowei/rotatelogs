package main

import (
	"fmt"

	"gitlab.com/alexia.shaowei/rotatelogs"
)

var path string = "log/"
var size int = 50

func main() {
	fmt.Println("main::main()")

	ftl, err := rotatelogs.New(rotatelogs.SYS_WINDOWS, rotatelogs.MODE_DEBUG, size, path)
	if err != nil {
		fmt.Printf("logger error: %v\n", err)
		return
	}

	ftl.Info("test")
}
