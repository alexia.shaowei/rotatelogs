package rotatelogs_test

//https://gitlab.com/alexia.shaowei/ftlogger

import (
	"testing"

	"gitlab.com/alexia.shaowei/rotatelogs"
)

var ftl *rotatelogs.RotateLog
var ftlErr error

func init() {
	ftl, ftlErr = rotatelogs.New(rotatelogs.SYS_WINDOWS, rotatelogs.MODE_SYSTEM, 5, "./UnitTestLog/")

}

/*
	=================   condition : console & file   =================
	BenchmarkLog-16             6400            187200 ns/op
	PASS
	coverage: 48.6% of statements
	ok      sw.logger/ftlogger      2.296s


	=================   condition : file only   ======================
	BenchmarkLog-16            32534             36196 ns/op
	PASS
	coverage: 43.2% of statements
	ok      sw.logger/ftlogger      1.540s


	=================   condition : console only   ======================
	BenchmarkLog-16            10842            118211 ns/op
	PASS
	coverage: 45.9% of statements
	ok      sw.logger/ftlogger      2.027s
*/

// go test -v -cover -short -bench .
func BenchmarkLog(b *testing.B) {
	// b.Skip()
	for i := 0; i < b.N; i++ {
		ftl.Warning("log unit test benchmark: %d", i)
	}

}

// go test -v -cover -short .
func TestLogInfo(t *testing.T) {
	t.Skip()

	for i := 0; i < 1000; i++ {
		ftl.Info("log unit test: %d", i)
	}

}
