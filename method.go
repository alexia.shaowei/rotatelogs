package rotatelogs

import (
	"fmt"
)

func (ref *RotateLog) CurrentInformation() string {
	mode := ref.mode
	currentFileName := ref.fileOutput.CurrentFileName()

	return fmt.Sprintf("FTLog mode: %d, current file name: %s", mode, currentFileName)
}

func (ref *RotateLog) Info(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVICE_INFO, LEVEL_INFO, ref.system, ref.mode, message, args...))
}

func (ref *RotateLog) Warning(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVICE_WARNING, LEVEL_WARNING, ref.system, ref.mode, message, args...))
}

func (ref *RotateLog) Error(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVICE_ERROR, LEVEL_ERROR, ref.system, ref.mode, message, args...))
}

func (ref *RotateLog) Panic(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVICE_PANIC, LEVEL_PANIC, ref.system, ref.mode, message, args...))
}

func (ref *RotateLog) Debug(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVICE_DEBUG, LEVEL_DEBUG, ref.system, ref.mode, message, args...))
}

// ************************     DON'T USE     ************************
func (ref *RotateLog) ServerInfo(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVER_INFO, LEVEL_INFO, ref.system, ref.mode, message, args...))
}

func (ref *RotateLog) ServerWarning(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVER_WARNING, LEVEL_WARNING, ref.system, ref.mode, message, args...))
}

func (ref *RotateLog) ServerError(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVER_ERROR, LEVEL_ERROR, ref.system, ref.mode, message, args...))
}

func (ref *RotateLog) ServerPanic(message string, args ...interface{}) {
	ref.rtlg.Println(makeMessage(CONTENT_SERVER_PANIC, LEVEL_PANIC, ref.system, ref.mode, message, args...))
}

// ************************     DON'T USE     ************************
