package rotatelogs

import (
	"fmt"
)

func makeMessage(content string, level int, systemColor int, mode int, msg string, args ...interface{}) string {

	outputColor := "%s"
	if systemColor == SYS_LINUX && (mode == MODE_DEBUG || mode == MODE_DEFAULT) {
		switch level {
		case LEVEL_INFO:
			outputColor = COLOR_CMD_GREEN
		case LEVEL_WARNING:
			outputColor = COLOR_CMD_YELLOW
		case LEVEL_ERROR:
			outputColor = COLOR_CMD_RED
		case LEVEL_PANIC:
			outputColor = COLOR_CMD_PURPLE
		case LEVEL_SYS_OPERATOR:
			outputColor = COLOR_CMD_CYAN
		case LEVEL_DEBUG:
			outputColor = COLOR_CMD_WHITE
		}
	}

	bindContent := fmt.Sprintf(outputColor, content)

	return fmt.Sprintf(bindContent, fmt.Sprintf(msg, args...))
}
