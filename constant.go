package rotatelogs

// Linux console output color
const (
	RESET        = 0
	COLOR_BLACK  = 30
	COLOR_RED    = 31
	COLOR_GREEN  = 32
	COLOR_YELLOW = 33
	COLOR_BLUE   = 34
	COLOR_PURPLE = 35
	COLOR_CYAN   = 36
	COLOR_GRAY   = 37
	COLOR_WHITE  = 97
)

const (
	COLOR_CMD_RESET  = "\033[0m%v"
	COLOR_CMD_RED    = "\x1b[0;31m%v\x1b[0m"
	COLOR_CMD_GREEN  = "\x1b[0;32m%v\x1b[0m"
	COLOR_CMD_YELLOW = "\x1b[0;33m%v\x1b[0m"
	COLOR_CMD_BLUE   = "\x1b[0;34m%v\x1b[0m"
	COLOR_CMD_PURPLE = "\x1b[0;35m%v\x1b[0m"
	COLOR_CMD_CYAN   = "\x1b[0;36m%v\x1b[0m"
	COLOR_CMD_GRAY   = "\x1b[0;37m%v\x1b[0m"
	COLOR_CMD_WHITE  = "\x1b[0;97m%v\x1b[0m"
)

const (
	LEVEL_INFO         = 0
	LEVEL_WARNING      = 1
	LEVEL_ERROR        = 2
	LEVEL_PANIC        = 3
	LEVEL_DEBUG        = 4
	LEVEL_SYS_OPERATOR = 5
)

const (
	LEVEL_MSG_INFO         = "INFO"
	LEVEL_MSG_WARNING      = "WARNING"
	LEVEL_MSG_ERROR        = "ERROR"
	LEVEL_MSG_PANIC        = "PANIC"
	LEVEL_MSG_DEBUG        = "DEBUG"
	LEVEL_MSG_SYS_OPERATOR = "OPERATOR"
)

const (
	MODE_DESC_DEFAULT = "DEFAULT"
	MODE_DESC_DEBUG   = "DEBUG"
	MODE_DESC_SYSTEM  = "SYSTEM"
	MODE_DESC_MQ      = "MQ"
	MODE_DESC_AWS     = "AWS"
)

const (
	CONTENT_FORMCOLOR       = "\033[%dm%v"
	CONTENT_DEFAULT         = "[DEFAULT] %s"
	CONTENT_SERVER_INFO     = "[SERVER INFO] %s"
	CONTENT_SERVER_WARNING  = "[SERVER WARNING] %s"
	CONTENT_SERVER_ERROR    = "[SERVER ERROR] %s"
	CONTENT_SERVER_PANIC    = "[SERVER PANIC] %s"
	CONTENT_SERVER_DEBUG    = "[SERVER DEBUG] %s"
	CONTENT_SERVER_OPERATOR = "[SERVER OPERATOR] %s"
	CONTENT_SERVICE_INFO    = "[INFO] %s"
	CONTENT_SERVICE_WARNING = "[WARNING] %s"
	CONTENT_SERVICE_ERROR   = "[ERROR] %s"
	CONTENT_SERVICE_PANIC   = "[PANIC] %s"
	CONTENT_SERVICE_DEBUG   = "[DEBUG] %s"
)

const (
	FILE_LOG     = "LOG-%v.log"
	FILE_SERVICE = "service_%Y%m%d.log"
)

const (
	FILE_MODE_SERVER  = 0
	FILE_MODE_SERVICE = 1
)

const (
	MODE_DEFAULT = 0 // output to console only
	MODE_SYSTEM  = 1 // output to file only
	MODE_DEBUG   = 2 // output to file and console
	// MODE_MQ      = 3
	// MODE_AWS     = 4
)

const (
	SYS_LINUX   = 0
	SYS_WINDOWS = 1
)
