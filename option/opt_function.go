package option

func New(optKey string, optValue interface{}) *Option {
	return &Option{
		optKey:   optKey,
		optValue: optValue,
	}
}
