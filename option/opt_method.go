package option

func (ref *Option) OptKey() string {
	return ref.optKey
}
func (ref *Option) OptValue() interface{} {
	return ref.optValue
}
