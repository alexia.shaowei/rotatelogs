package strftime

var keywordExclusion []string = []string{
	"MST",
	"PM",
	"pm",
	"Mon",
	"Monday",
	"Jan",
	"January",
}

var (
	dayOfYear            = AppendFunc(appendDayOfYear)
	fullWeekDayName      = StdlFormat("Monday")
	abbrvWeekDayName     = StdlFormat("Mon")
	fullMonthName        = StdlFormat("January")
	abbrvMonthName       = StdlFormat("Jan")
	timeAndDate          = StdlFormat("Mon Jan _2 15:04:05 2006")
	mdy                  = StdlFormat("01/02/06")
	dayOfMonthZeroPad    = StdlFormat("02")
	dayOfMonthSpacePad   = StdlFormat("_2")
	ymd                  = StdlFormat("2006-01-02")
	natReprTime          = StdlFormat("15:04:05")
	natReprDate          = StdlFormat("01/02/06")
	year                 = StdlFormat("2006")
	yearNoCentury        = StdlFormat("06")
	timezone             = StdlFormat("MST")
	timezoneOffset       = StdlFormat("-0700")
	minutesZeroPad       = StdlFormat("04")
	monthNumberZeroPad   = StdlFormat("01")
	ampm                 = StdlFormat("PM")
	hm                   = StdlFormat("15:04")
	secondsNumberZeroPad = StdlFormat("05")
	hms                  = StdlFormat("15:04:05")
	eby                  = StdlFormat("_2-Jan-2006")
	newline              = Verbatimer("\n")
	tab                  = Verbatimer("\t")
	percent              = Verbatimer("%")
)

var defaultSpecifications = map[byte]Appender{
	'A': fullWeekDayName,
	'a': abbrvWeekDayName,
	'B': fullMonthName,
	'b': abbrvMonthName,
	'c': timeAndDate,
	'D': mdy,
	'd': dayOfMonthZeroPad,
	'e': dayOfMonthSpacePad,
	'F': ymd,
	'h': abbrvMonthName,
	'j': dayOfYear,
	'M': minutesZeroPad,
	'm': monthNumberZeroPad,
	'n': newline,
	'p': ampm,
	'R': hm,
	'S': secondsNumberZeroPad,
	'T': hms,
	't': tab,
	'v': eby,
	'X': natReprTime,
	'x': natReprDate,
	'Y': year,
	'y': yearNoCentury,
	'Z': timezone,
	'z': timezoneOffset,
	'%': percent,
}
