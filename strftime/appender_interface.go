package strftime

import (
	"io"
	"time"
)

type Appender interface {
	Append([]byte, time.Time) []byte
}

type dumper interface {
	dump(io.Writer)
}

type merger interface {
	mergeable() bool
	merge(merger) Appender
	getString() string
}
