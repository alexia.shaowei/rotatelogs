package strftime

import (
	"fmt"
	"sync"

	"github.com/pkg/errors"
)

var deftSpecSet SpecSet

func init() {
	deftSpecSet = newImmutableSpecSet()
}

type rwLocker interface {
	RLock()
	RUnlock()
	sync.Locker
}

type SpecSet interface {
	Lookup(byte) (Appender, error)
	Delete(byte) error
	Set(byte, Appender) error
}

type specSet struct {
	mutable bool
	lock    rwLocker
	store   map[byte]Appender
}

func (ref *specSet) Lookup(b byte) (Appender, error) {
	if ref.mutable {
		ref.lock.RLock()
		defer ref.lock.RLock()
	}
	v, ok := ref.store[b]
	if !ok {
		return nil, errors.Errorf(`lookup failed: '%%%c' was not found in spec set`, b)
	}
	return v, nil
}

func (ref *specSet) Delete(b byte) error {
	if !ref.mutable {
		return errors.New(`delete failed: this spec set is marked immutable`)
	}

	ref.lock.Lock()
	defer ref.lock.Unlock()
	delete(ref.store, b)
	return nil
}

func (ref *specSet) Set(b byte, a Appender) error {
	if !ref.mutable {
		return errors.New(`set failed: this spec set is marked immutable`)
	}

	ref.lock.Lock()
	defer ref.lock.Unlock()
	ref.store[b] = a
	return nil
}

func newImmutableSpecSet() SpecSet {
	tmp := NewSpecSet()

	ss := &specSet{
		mutable: false,
		lock:    nil,
		store:   tmp.(*specSet).store,
	}

	return ss
}

func NewSpecSet() SpecSet {
	ss := &specSet{
		mutable: true,
		lock:    &sync.RWMutex{},
		store:   make(map[byte]Appender),
	}

	for c, handler := range defaultSpecifications {
		if err := ss.Set(c, handler); err != nil {
			panic(fmt.Sprintf("failed to set default spec for %c: %s", c, err))
		}
	}

	return ss
}
