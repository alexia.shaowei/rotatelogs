package rotatelogs

import (
	"log"

	rotatefiles "gitlab.com/alexia.shaowei/rotatelogs/rotatefiles"
)

type RotateLog struct {
	rtlg       *log.Logger
	fileOutput *rotatefiles.RotateLogs

	mode   int
	system int
}
