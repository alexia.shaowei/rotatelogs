package rotatelogs

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/alexia.shaowei/rotatelogs/rotatefiles"
)

func New(system int, mode int, size int, logRootPath string) (*RotateLog, error) {

	timeFormat := log.Ldate | log.Ltime //time format "2021/05/06 10:26:47"

	var lgr *log.Logger

	path := logRootPath + FILE_LOG

	rttFiles, err := rotatefiles.New(
		path,
		rotatefiles.MaxAge(-1),
		rotatefiles.RotationSize(1024*1000*int64(size)),
		rotatefiles.RotationTime(time.Hour*24),
	)

	if err != nil {
		FTL = nil
		return nil, fmt.Errorf("rotate file error")
	}

	if mode == MODE_DEBUG {
		lgr = log.New(
			io.MultiWriter(os.Stdout, rttFiles),
			"",
			timeFormat,
		)

	} else if mode == MODE_SYSTEM {
		lgr = log.New(
			rttFiles,
			"",
			timeFormat,
		)

	} else {
		lgr = log.New(
			os.Stdout,
			"",
			timeFormat,
		)
	}

	FTL = &RotateLog{
		rtlg:       lgr,
		fileOutput: rttFiles,
		mode:       mode,
		system:     system,
	}

	return FTL, nil
}
