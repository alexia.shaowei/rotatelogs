package rotatefiles

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/alexia.shaowei/rotatelogs/strftime"
)

var patternConversionRegexps = []*regexp.Regexp{
	regexp.MustCompile(`%[%+A-Za-z]`),
	regexp.MustCompile(`\*+`),
}

type RotateLogs struct {
	clock         ClockIX
	curFn         string
	curBaseFn     string
	globPattern   string
	generation    int
	linkName      string
	maxAge        time.Duration
	mutex         sync.RWMutex
	eventHandler  HandlerIX
	outFh         *os.File
	pattern       *strftime.StringFormatTime
	rotationTime  time.Duration
	rotationSize  int64
	rotationCount uint
	forceNewFile  bool
}

type clockFn func() time.Time

var Local = clockFn(time.Now)

func (ref clockFn) Now() time.Time {
	return ref()
}

func New(p string, options ...OptionIX) (*RotateLogs, error) {
	rotationTime := 24 * time.Hour
	globPattern := p

	for _, re := range patternConversionRegexps {
		globPattern = re.ReplaceAllString(globPattern, "*")
	}

	pattern, err := strftime.New(p)
	if err != nil {
		return nil, errors.Wrap(err, "invalid pattern")
	}

	var clock ClockIX = Local

	var rotationSize int64
	var rotationCount uint
	var linkName string
	var maxAge time.Duration
	var handler HandlerIX
	var forceNewFile bool

	for _, opts := range options {
		switch opts.OptKey() {
		case optkeyClock:
			clock = opts.OptValue().(ClockIX)

		case optkeyLinkName:
			linkName = opts.OptValue().(string)

		case optkeyMaxAge:
			maxAge = opts.OptValue().(time.Duration)
			if maxAge < 0 {
				maxAge = 0
			}

		case optkeyRotationTime:
			rotationTime = opts.OptValue().(time.Duration)
			if rotationTime < 0 {
				rotationTime = 0
			}

		case optkeyRotationSize:
			rotationSize = opts.OptValue().(int64)
			if rotationSize < 0 {
				rotationSize = 0
			}

		case optkeyRotationCount:
			rotationCount = opts.OptValue().(uint)

		case optkeyHandler:
			handler = opts.OptValue().(HandlerIX)

		case optkeyForceNewFile:
			forceNewFile = true
		}
	}

	if maxAge > 0 && rotationCount > 0 {
		return nil, errors.New(`maxAge and rotationCount can not both set`)
	}

	if maxAge == 0 && rotationCount == 0 {
		maxAge = 360 * 24 * time.Hour
	}

	return &RotateLogs{
		clock:         clock,
		eventHandler:  handler,
		globPattern:   globPattern,
		linkName:      linkName,
		maxAge:        maxAge,
		pattern:       pattern,
		rotationTime:  rotationTime,
		rotationSize:  rotationSize,
		rotationCount: rotationCount,
		forceNewFile:  forceNewFile,
	}, nil
}

func (ref *RotateLogs) getFileName() string {
	return ref.pattern.FormatString(ref.clock.Now().Truncate(time.Duration(ref.rotationTime)))
}

func (ref *RotateLogs) Write(p []byte) (n int, err error) {
	ref.mutex.Lock()
	defer ref.mutex.Unlock()

	out, err := ref.getWriterWithoutLock(false, false)
	if err != nil {
		return 0, errors.Wrap(err, `failed to acquite target io.Writer`)
	}

	return out.Write(p)
}

func (ref *RotateLogs) getWriterWithoutLock(bailOnRotateFail, useGenerationalNames bool) (io.Writer, error) {
	generation := ref.generation
	previousFn := ref.curFn

	baseFn := ref.getFileName()
	filename := baseFn
	var forceNewFile bool

	fi, err := os.Stat(ref.curFn)
	sizeRotation := false
	if err == nil && ref.rotationSize > 0 && ref.rotationSize <= fi.Size() {
		forceNewFile = true
		sizeRotation = true
	}

	if baseFn != ref.curBaseFn {
		generation = 0

		if ref.forceNewFile {
			forceNewFile = true
		}
	} else {
		if !useGenerationalNames && !sizeRotation {
			return ref.outFh, nil
		}
		forceNewFile = true
		generation++
	}

	if forceNewFile {

		var name string
		for {
			if generation == 0 {
				name = filename
			} else {
				lastIdx := strings.LastIndex(filename, ".")
				if lastIdx > 0 {
					name = fmt.Sprintf("%s_%d%s", filename[:lastIdx], generation, filename[lastIdx:])
					// fmt.Printf("name: %v, lastIdx: %v\n", name, lastIdx)
				} else {
					name = fmt.Sprintf("%s.%d", filename, generation)
					// fmt.Printf("name: %v, lastIdx: %v\n", name, lastIdx)
				}

			}
			if _, err := os.Stat(name); err != nil {
				filename = name
				break
			}
			generation++
		}
	}

	dirname := filepath.Dir(filename)
	if err := os.MkdirAll(dirname, 0755); err != nil {
		return nil, errors.Wrapf(err, "failed to create dir %s", dirname)
	}

	fh, err := os.OpenFile(filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return nil, errors.Errorf("failed to open file %s: %s", ref.pattern, err)
	}

	if err := ref.rotateWithoutLock(filename); err != nil {
		err = errors.Wrap(err, "failed to rotate")
		if bailOnRotateFail {
			if fh != nil {
				fh.Close()
			}

			return nil, err
		}
		fmt.Fprintf(os.Stderr, "%s\n", err.Error())
	}

	ref.outFh.Close()
	ref.outFh = fh
	ref.curBaseFn = baseFn
	ref.curFn = filename
	ref.generation = generation

	if h := ref.eventHandler; h != nil {
		go h.Handle(&FileRotatedEvent{
			prev:    previousFn,
			current: filename,
		})
	}
	return fh, nil
}

func (ref *RotateLogs) CurrentFileName() string {
	ref.mutex.RLock()
	defer ref.mutex.RUnlock()
	return ref.curFn
}

func (ref *RotateLogs) Rotate() error {
	ref.mutex.Lock()
	defer ref.mutex.Unlock()
	if _, err := ref.getWriterWithoutLock(true, true); err != nil {
		return err
	}
	return nil
}

func (ref *RotateLogs) rotateWithoutLock(filename string) error {
	lockfn := filename + `_lock`
	fh, err := os.OpenFile(lockfn, os.O_CREATE|os.O_EXCL, 0644)
	if err != nil {
		return err
	}

	var cu cleaner

	cu.fn = func() {
		fh.Close()
		os.Remove(lockfn)
	}
	defer cu.Run()

	if ref.linkName != "" {
		tmpLinkName := filename + `_symlink`

		linkDest := filename
		linkDir := filepath.Dir(ref.linkName)

		baseDir := filepath.Dir(filename)
		if strings.Contains(ref.linkName, baseDir) {
			tmp, err := filepath.Rel(linkDir, filename)
			if err != nil {
				return errors.Wrapf(err, `failed to evaluate relative path from %#v to %#v`, baseDir, ref.linkName)
			}

			linkDest = tmp
		}

		if err := os.Symlink(linkDest, tmpLinkName); err != nil {
			return errors.Wrap(err, `failed to create new symlink`)
		}

		_, err := os.Stat(linkDir)
		if err != nil {
			if err := os.MkdirAll(linkDir, 0755); err != nil {
				return errors.Wrapf(err, `failed to create directory %s`, linkDir)
			}
		}

		if err := os.Rename(tmpLinkName, ref.linkName); err != nil {
			return errors.Wrap(err, `failed to rename new symlink`)
		}
	}

	if ref.maxAge <= 0 && ref.rotationCount <= 0 {
		return errors.New("panic: maxAge and rotationCount are both set")
	}

	matches, err := filepath.Glob(ref.globPattern)
	if err != nil {
		return err
	}

	cutoff := ref.clock.Now().Add(-1 * ref.maxAge)

	var toUnlink []string

	for _, path := range matches {

		if strings.HasSuffix(path, "_lock") || strings.HasSuffix(path, "_symlink") {
			continue
		}

		fi, err := os.Stat(path)
		if err != nil {
			continue
		}

		fl, err := os.Lstat(path)
		if err != nil {
			continue
		}

		if ref.maxAge > 0 && fi.ModTime().After(cutoff) {
			continue
		}

		if ref.rotationCount > 0 && fl.Mode()&os.ModeSymlink == os.ModeSymlink {
			continue
		}

		toUnlink = append(toUnlink, path)
	}

	if ref.rotationCount > 0 {

		if ref.rotationCount >= uint(len(toUnlink)) {
			return nil
		}

		toUnlink = toUnlink[:len(toUnlink)-int(ref.rotationCount)]
	}

	if len(toUnlink) <= 0 {
		return nil
	}

	cu.Enable()
	go func() {
		for _, path := range toUnlink {
			os.Remove(path)
		}
	}()

	return nil
}

func (ref *RotateLogs) Close() error {
	ref.mutex.Lock()
	defer ref.mutex.Unlock()

	if ref.outFh == nil {
		return nil
	}

	ref.outFh.Close()
	ref.outFh = nil
	return nil
}
