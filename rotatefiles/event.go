package rotatefiles

type EventType int

const (
	InvalidEventType EventType = iota
	FileRotatedEventType
)

type HandlerFunc func(EventIX)

func (ref HandlerFunc) Handle(e EventIX) {
	ref(e)
}

type FileRotatedEvent struct {
	prev    string
	current string
}

func (ref *FileRotatedEvent) Type() EventType {
	return FileRotatedEventType
}

func (ref *FileRotatedEvent) PreviousFile() string {
	return ref.prev
}

func (ref *FileRotatedEvent) CurrentFile() string {
	return ref.current
}
