package rotatefiles

import (
	"time"

	"gitlab.com/alexia.shaowei/rotatelogs/option"
)

const (
	optkeyClock         = "CLOCK"
	optkeyHandler       = "HANDLER"
	optkeyLinkName      = "LINKNAME"
	optkeyMaxAge        = "MAXAGE"
	optkeyRotationTime  = "ROTATION_TIME"
	optkeyRotationSize  = "ROTATION_SIZE"
	optkeyRotationCount = "ROTATION_COUNT"
	optkeyForceNewFile  = "FORCE_NEW_FILE"
)

func WithClock(c ClockIX) OptionIX {
	return option.New(optkeyClock, c)
}

func WithLocation(loc *time.Location) OptionIX {
	return option.New(
		optkeyClock,
		clockFn(
			func() time.Time {
				return time.Now().In(loc)
			},
		))
}

func LinkName(s string) OptionIX {
	return option.New(optkeyLinkName, s)
}

func MaxAge(d time.Duration) OptionIX {
	return option.New(optkeyMaxAge, d)
}

func RotationTime(d time.Duration) OptionIX {
	return option.New(optkeyRotationTime, d)
}

func RotationSize(s int64) OptionIX {
	return option.New(optkeyRotationSize, s)
}

func RotationCount(n uint) OptionIX {
	return option.New(optkeyRotationCount, n)
}

func Handler(h HandlerIX) OptionIX {
	return option.New(optkeyHandler, h)
}

func ForceCreate() OptionIX {
	return option.New(optkeyForceNewFile, true)
}
